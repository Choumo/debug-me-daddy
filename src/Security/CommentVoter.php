<?php

namespace App\Security;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CommentVoter extends Voter
{

    const VIEW = 'view';
    const DELETE = 'delete';
    const EDIT = 'edit';
    const CREATE = 'create';

    public function __construct(private readonly Security $security)
    {

    }

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if(!in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        if(!$subject instanceof Comment) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        //Check if user have "ROLE_ADMIN" (Admins can do anything they want)
        if($this->security->isGranted(attributes: 'ROLE_ADMIN')){
            return true;
        }

        // Check if user is logged, if not, he does not have access
        if(!$user instanceof User) {
            return false;
        }

        $comment = $subject;

        return match ($attribute) {
            self::DELETE => $this->canDelete($comment, $user),
            self::EDIT => $this->canEdit($comment, $user),
        };
    }

    private function canDelete(Comment $comment, User $user): bool
    {
        return $user === $comment->getAuthor();
    }

    private function canEdit(Comment $comment, User $user): bool
    {
        return $user === $comment->getAuthor();
    }
}
