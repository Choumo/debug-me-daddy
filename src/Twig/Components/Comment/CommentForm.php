<?php

namespace App\Twig\Components\Comment;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class CommentForm extends AbstractController
{
    use ComponentWithFormTrait;
    use DefaultActionTrait;

    #[LiveProp]
    public bool $isFormValid = false;

    #[LiveProp]
    public ?Comment $initialFormData = null;

    public Ticket $ticket;


    protected function instantiateForm(): FormInterface
    {
        if ($this->initialFormData === null) {
            $this->initialFormData = new Comment();
            $this->initialFormData->setTicket($this->ticket);
        }

        return $this->createForm(CommentType::class, $this->initialFormData);
    }

    public function hasValidationErrors(): bool
    {
        $this->isFormValid = $this->getForm()->isSubmitted() && !$this->getForm()->isValid();

        return $this->isFormValid;
    }

    #[LiveAction]
    public function create(EntityManagerInterface $entityManager)
    {
        $this->submitForm();

        $comment = $this->getForm()->getData();
        $entityManager->persist($comment);
        $entityManager->flush();
    }
}
