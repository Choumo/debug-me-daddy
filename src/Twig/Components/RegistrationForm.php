<?php

namespace App\Twig\Components;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

/**
 * Composant "Live" générant un formulaire d'enregistrement et validant en temps réel les différents champs basés sur
 * les contraintes définies dans les entités et les données saisies par l'utilisateur
 */
#[AsLiveComponent]
class RegistrationForm extends AbstractController
{
    use ComponentWithFormTrait;
    use DefaultActionTrait;

    #[LiveProp]
    public bool $isFormValid = false;

    #[LiveProp]
    public ?User $initialFormData = null;

    /**
     * Fonction générant le formulaire d'enregistrement
     * @return FormInterface
     */
    protected function instantiateForm(): FormInterface
    {
        return $this->createForm(RegistrationFormType::class, $this->initialFormData);
    }

    /**
     * Fonction permettant de vérifier la validité du formulaire
     * @return bool
     */
    public function hasValidationErrors(): bool
    {
        $this->isFormValid = $this->getForm()->isSubmitted() && !$this->getForm()->isValid();

        return $this->isFormValid;
    }

    /**
     * Fonction permettant d'enregistrer l'utilisateur en base de données si tous les champs sont corrects
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|void
     */
    #[LiveAction]
    public function register(UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager)
    {
        try {
            $this->submitForm();

            $user = $this->getForm()->getData();

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Your account has been created, you can now log in');

            $this->redirectToRoute('app_login');

            return $this->redirectToRoute(route: 'app_login');
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash('error', $exception->getMessage());
        }
    }
}
