<?php

namespace App\Twig\Components\Administration;

use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\ComponentToolsTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class AdministrationTagCard extends AbstractController
{
    use DefaultActionTrait;
    use ComponentToolsTrait;

    public ?Tag $tag = null;

    #[LiveAction]
    public function selectTag(#[LiveArg] Tag $tag, EntityManagerInterface $entityManager): void
    {
        $tagRepository = $entityManager->getRepository(Tag::class);
        $this->tag = $tagRepository->find($tag);

        $this->emit('productAdded', [
            'product' => $tag->getId(),
        ]);
    }
}
