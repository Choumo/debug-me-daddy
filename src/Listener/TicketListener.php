<?php

namespace App\Listener;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * This class will listen for any persist or update to a Ticket
 */
#[AsEntityListener(
    event: Events::prePersist,
    method: 'prePersist',
    entity: Ticket::class
)]
#[AsEntityListener(
    event: Events::preUpdate,
    method: 'preUpdate',
    entity: Ticket::class
)]
class TicketListener extends AbstractListener
{

    /**
     *  This function is executed before a ticket is persisted.
     *  This function will automatically set the date and time of creation, the author of the ticket
     *  and add a point to this user.
     *
     *  However, if we call the command "symfony console doctrine:fixtures:load", an error will be triggered because there is no logged user.
     *  To prevent any error, we add a condition to check if php has been call via a CLI.
     * @param Ticket $ticket
     * @param PrePersistEventArgs $eventArgs
     * @return void
     */
    public function prePersist(Ticket $ticket, PrePersistEventArgs $eventArgs): void
    {
        $ticket->setCreatedAt(
            new \DateTimeImmutable('now')
        );

        if('cli' != php_sapi_name()){
            $ticket->setAuthor(
                $this->security->getUser()
            );

            $this->addPointToUser();
        }
    }

    /**
     * This function is executed before a ticket is updated.
     * This function will add the updated date and time of the ticket.
     * @param Ticket $ticket
     * @param PreUpdateEventArgs $eventArgs
     * @return void
     */
    public function preUpdate(Ticket $ticket, PreUpdateEventArgs $eventArgs): void
    {
        $ticket->setUpdatedAt(
            new \DateTimeImmutable('now')
        );
    }



}
