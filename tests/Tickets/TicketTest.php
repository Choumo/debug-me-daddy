<?php

namespace App\Tests\Tickets;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TicketTest extends WebTestCase
{

    public function testAccessTicketListUserDisconnected(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testAccessTicketListUserConnectedRegular(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre0@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testAccessTicketListUserConnectedAdmin(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre-admin@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testAccessTicketDetailsUserDisconnected(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testAccessTicketDetailsUserRegular(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre0@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testAccessTicketDetailsUserAdmin(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre0@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testDeleteTicketUserDisconnected(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testDeleteTicketUserConnectedAndNotAuthorOfTicket(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre1@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testDeleteTicketUserConnectedAndIsAuthorOfTicket(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre0@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }

    public function testDeleteTicketUserAdmin(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['email' => 'alexandre-admin@gmail.com']);

        $client->loginUser($user);
        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
    }
}
